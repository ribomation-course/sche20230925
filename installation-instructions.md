# Installation Instructions

In order to participate and perform the programming exercises of the course, 
you need to have the following installed.


## GIT Client
* https://git-scm.com/downloads

## IDE
A decent IDE, such as any of

* JetBrains IntelliJ IDEA (*Community*)
    - https://www.jetbrains.com/idea/download
* MicroSoft Visual Code
    - https://code.visualstudio.com/download


## Java JDK
Install a recent version of Java JDK from Oracle
* https://www.oracle.com/java/technologies/downloads/

Or; Install SDKMAN https://sdkman.io/ to GIT BASH

    curl -s "https://get.sdkman.io" | bash
    source "$HOME/.sdkman/bin/sdkman-init.sh"
    sdk version

and use it to install the latest version of Java

    sdk install java

More info https://sdkman.io/usage

