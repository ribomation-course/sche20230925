package ribomation;

public class ProcedureDivision {
    public static void main(String[] args) {
        var app = new ProcedureDivision();
        app.perform_section_doit();
    }

    private void perform_section_doit() {
        var x = DataDivision.count;
        System.out.printf("x = %d%n", x);
    }
}
