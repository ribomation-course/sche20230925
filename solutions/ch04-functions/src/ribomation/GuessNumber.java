package ribomation;

import java.util.Scanner;

public class GuessNumber {
    public static void main(String[] args) {
        var app = new GuessNumber();
        app.run();
    }

    private void run() {
        var secret = (int)(Math.random()*10 + 1);
        var in     = new Scanner(System.in); //remember to import it
        var count  = 0;
        var guess  = 0;
        do {
            System.out.print("Guess: ");
            guess = in.nextInt();
            ++count;
        } while (guess != secret && count < 3);
        if (guess == secret) System.out.println("CORRECT!");
        else System.out.printf("secret = %d%n", secret);
    }
}
