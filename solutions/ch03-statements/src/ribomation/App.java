package ribomation;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        var arr = new int[5];
        System.out.println("Give 5 int numbers: ");
        var in = new Scanner(System.in);
        for (var k = 0; k < arr.length; ++k) {
            arr[k] = in.nextInt();
        }

        // (1)
        for (var n : arr) System.out.printf("%d ", n);
        System.out.println();

        // (2)
        var sum = 0;
        for (var n : arr) sum = sum + n;
        System.out.printf("sum = %d%n", sum);

        // (3)
        var prod = 1;
        for (var n : arr) prod *= n;
        System.out.printf("prod = %d%n", prod);

        // (4)
        var minValue = Integer.MAX_VALUE;
//        for (var n : arr) minValue = n < minValue ? n : minValue;
        for (var n : arr) {
            if (n < minValue) minValue = n;
            else minValue = minValue;
        }
        System.out.printf("min = %d%n", minValue);

        // (5)
        var maxValue = Integer.MIN_VALUE;
        for (var n : arr) maxValue = n > maxValue ? n : maxValue;
        System.out.printf("max = %d%n", maxValue);

        // (6)
        System.out.printf("Avg: %f%n", (double) sum / arr.length);
    }
}
