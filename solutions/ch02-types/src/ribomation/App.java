package ribomation;
public class App {
    public static void main(String[] args) {
        var N = (args.length == 0) ? 10 : Integer.parseInt(args[0]);
        var result = N * (N + 1) / 2;
        System.out.printf("SUM(1..%d) = %d%n", N, result);
    }
}
