package ribomation;

public class Triang extends Shape {
    private final int b;
    private final int h;

    public Triang(int b, int h) {
        super();
        this.b = b;
        this.h = h;
    }

    @Override
    public double area() {
        return b * h / 2.0;
    }

    @Override
    public String toString() {
        return "Triang{" +
               "b=" + b +
               ", h=" + h +
               "} area=" + area();
    }
}
