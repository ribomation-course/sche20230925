package ribomation;

import java.util.Random;

public class Shapes {
    public static void main(String[] args) {
        var app = new Shapes();
        app.run();
    }

    private void run() {
        Shape[] shapes = mkShapes(100);
        for (var s : shapes) {
            System.out.println(s);
        }
    }

    Shape[] mkShapes(int n) {
        Shape[] arr = new Shape[n];
        for (int k = 0; k < arr.length; k++) {
            arr[k] = mkShape();
        }
        return arr;
    }

    Shape mkShape() {
        var r = new Random();
        switch (r.nextInt(3)) {
            case 0 -> {
                return new Rect(r.nextInt(10), r.nextInt(12));
            }
            case 1 -> {
                return new Triang(r.nextInt(10), r.nextInt(12));
            }
            case 2 -> {
                return new Circ(r.nextInt(17));
            }
            default -> {
                return null;
            }
        }
    }
}
