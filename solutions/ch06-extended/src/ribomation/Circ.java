package ribomation;

public class Circ extends Shape {
    private final int r;

    public Circ(int r) {
        super();
        this.r = r;
    }

    @Override
    public double area() {
        return Math.PI * r * r;
    }

    @Override
    public String toString() {
        return "Circ{" +
               "r=" + r +
               "} area=" + area();
    }
}
