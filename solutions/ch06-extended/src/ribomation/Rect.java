package ribomation;

public class Rect extends Shape {
    private final int w;
    private final int h;

    public Rect(int w_, int h) {
        super();
        w = w_;
        this.h = h;
    }

    @Override
    public double area() {
        return w * h;
    }

    @Override
    public String toString() {
        return "Rect{" +
               "w=" + w +
               ", h=" + h +
               "} area=" + area();
    }
}
