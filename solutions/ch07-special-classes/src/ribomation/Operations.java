package ribomation;

public enum Operations {
    add('+'), sub('-'), mul('*'), div('/');

    private final char op;

    Operations(char op) {
        this.op = op;
    }

    public static Operations from(char op) {
        return switch (op) {
            case '+' -> add;
            case '-' -> sub;
            case '*' -> mul;
            case '/' -> div;
            default -> throw new IllegalArgumentException("Unknown operator: " + op);
        };
    }

    public static double eval(double lhs, Operations op, double rhs) {
        return switch (op) {
            case add -> lhs + rhs;
            case sub -> lhs - rhs;
            case mul -> lhs * rhs;
            case div -> lhs / rhs;
        };
    }
}
