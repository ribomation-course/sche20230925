package ribomation;

import java.util.Scanner;

import static ribomation.Operations.eval;

public class App {
    public static void main(String[] args) {
        new App().run();
    }

    private void run() {
        var in = new Scanner(System.in);
        while (true) {
            System.out.print("Enter expression: ");
            var lhs = in.nextDouble();
            var op = in.next().charAt(0);
            var rhs = in.nextDouble();
            System.out.printf("%f %c %f = %f%n", lhs, op, rhs, eval(lhs, Operations.from(op), rhs));
        }
    }
}
