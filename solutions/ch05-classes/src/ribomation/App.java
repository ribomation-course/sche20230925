package ribomation;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run();
    }

    private void run() {
        var nisse = new Person("Nisse Hult");
        System.out.printf("nisse: %s%n", nisse);

        var volvo = new Car("ABC123", "Volvo Sonnet");
        System.out.printf("volvo: %s%n", volvo);

        nisse.setMyCar(volvo);
        System.out.printf("nisse: %s%n", nisse);
        System.out.println("--------------");
        var d = new Dummy();
        System.out.printf("dummy: %s%n", d);
        System.out.printf("dummy: %s%n", d.tostring());
    }
}
