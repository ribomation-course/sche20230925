package ribomation.api;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Codec {
    public String encode(Object obj) {
        var cls = obj.getClass();
        var rec = cls.getAnnotation(Record.class);
        if (rec == null) {
            throw new IllegalArgumentException("Not a Record: " + cls);
        }

        return Stream.of(cls.getDeclaredFields())
                .filter(f -> f.isAnnotationPresent(Value.class))
                .map(field -> {
                    try {
                        var value = field.get(obj);
                        var type = field.getType();
                        if (type != String.class) {
                            value = String.valueOf(value);
                        }
                        var text = value.toString();

                        var val = field.getAnnotation(Value.class);
                        var size = val.size();
                        var pad = val.pad();

                        var buf = new char[size];
                        Arrays.fill(buf, pad);
                        for (var k = 0; k < text.length() && k < buf.length; k++) {
                            buf[k] = text.charAt(k);
                        }

                        return new String(buf);
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.joining());
    }

    public <T> T decode(final String input, Class<T> cls) {
        var rec = cls.getAnnotation(Record.class);
        if (rec == null) {
            throw new IllegalArgumentException("Not a Record: " + cls);
        }

        try {
            var con = cls.getConstructor();
            var obj = con.newInstance();
            final int[] start = {0};
            Stream.of(cls.getDeclaredFields())
                    .forEach(field -> {
                        try {
                            var val = field.getAnnotation(Value.class);
                            var size = val.size();
                            var text = input.substring(start[0], start[0] + size);
                            //System.out.printf("text = %s, start=%d, size=%d%n", text, start[0], size);
                            start[0] += size;

                            if (val.pad() != ' ') {
                                text = text.replace(val.pad(), ' ');
                            }
                            text = text.trim();

                            var type = field.getType();
                            if (type == String.class) {
                                field.set(obj, text);
                            } else if (type == int.class) {
                                field.setInt(obj, Integer.parseInt(text));
                            } else {
                                throw new IllegalArgumentException("Unsupported type: " + type);
                            }
                        } catch (IllegalAccessException e) {
                            throw new RuntimeException(e);
                        }
                    });

            return obj;
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException |
                 InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

}
