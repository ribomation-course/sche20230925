package ribomation;

import ribomation.api.Codec;
import ribomation.domain.Person;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class SimpleRead {
    public static void main(String[] args) throws IOException {
        var app = new SimpleRead();

        var file = "persons.dat";
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-f")) {
                file = args[++i];
            }
        }

        app.run(file);
    }

    void run(String file) throws IOException {
        var codec = new Codec();
        var lines = Files.lines(Path.of(file));
        try (lines) {
            lines
                    .map(line -> codec.decode(line, Person.class))
                    .forEach(p -> {
                        System.out.println("------");
                        System.out.printf("NAME: %s%n", p.name);
                        System.out.printf("EMAIL: %s%n", p.email);
                        System.out.printf("AGE: %s%n", p.age);
                        System.out.printf("COMPANY: %s%n", p.company);
                        System.out.printf("CITY: %s%n", p.city);
                    });
        }
    }

}
