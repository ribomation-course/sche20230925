package ribomation.domain;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Random;

public class PersonGenerator {
    final List<String> names;
    final List<String> emails;
    final List<String> companies;
    final List<String> cities;
    final Random r = new Random();

    public PersonGenerator(Path data_dir) throws IOException {
        names = load(data_dir, "names.txt");
        emails = load(data_dir, "emails.txt");
        companies = load(data_dir, "companies.txt");
        cities = load(data_dir, "cities.txt");
    }

    private static List<String> load(Path data_dir, String file) throws IOException {
        return Files.readAllLines(data_dir.resolve(file));
    }

    private String pick(List<String> list) {
        return list.get(r.nextInt(list.size()));
    }

    private int pick(int min, int max) {
        return r.nextInt(max - min) + min;
    }

    public Person next() {
        return new Person(
                pick(names),
                pick(emails),
                pick(20, 80),
                pick(companies),
                pick(cities)
        );
    }

}
