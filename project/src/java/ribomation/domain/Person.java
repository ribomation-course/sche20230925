package ribomation.domain;

import ribomation.api.Record;
import ribomation.api.Value;

import java.util.Objects;
import java.util.StringJoiner;

@Record
public class Person {
    @Value(size = 16) public String name;

    @Value(size = 20) public String email;

    @Value(size = 4) public int age;

    @Value(size = 18) public String company;

    @Value(size = 12) public String city;

    public Person() {}
    public Person(String name, String email, int age, String company, String city) {
        this.name = name;
        this.email = email;
        this.age = age;
        this.company = company;
        this.city = city;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Person.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("email='" + email + "'")
                .add("age=" + age)
                .add("company='" + company + "'")
                .add("city='" + city + "'")
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age && Objects.equals(name, person.name) && Objects.equals(email, person.email) && Objects.equals(company, person.company) && Objects.equals(city, person.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, email, age, company, city);
    }
}
