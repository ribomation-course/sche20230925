package ribomation.domain;

import ribomation.api.Codec;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class GenerateCobolFile {
    public static void main(String[] args) throws IOException {
        var app = new GenerateCobolFile();

        var N = 10;
        var outfile = "persons.dat";

        //java -cp classes ribomation.domain.GenerateCobolFile -n 1000 -o persons.dat
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-n")) {
                N = Integer.parseInt(args[++i]);
            } else if (args[i].equals("-o")) {
                outfile = args[++i];
            }
        }

        app.run(N, outfile);
    }

    void run(int N, String file) throws IOException {
        var gen = new PersonGenerator(Path.of("src/data"));
        var codec = new Codec();

        try (var out = Files.newBufferedWriter(Path.of(file))) {
            for (int i = 0; i < N; i++) {
                out.write(codec.encode(gen.next()));
                out.newLine();
            }
        }
    }
}
