IDENTIFICATION DIVISION.
PROGRAM-ID. ReadPersons.

ENVIRONMENT DIVISION.
    INPUT-OUTPUT SECTION.
        FILE-CONTROL.
        SELECT PersonFile ASSIGN TO 'persons.dat'
        ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
    FILE SECTION.
    FD PersonFile.
    01 PersonRecord.
        05 Name     PIC X(16).
        05 Email    PIC X(20).
        05 Age      PIC 9(4).
        05 Company  PIC X(18).
        05 City     PIC X(12).

    WORKING-STORAGE SECTION.
    01 WS-EOF       PIC X VALUE 'N'.

PROCEDURE DIVISION.
    OPEN INPUT PersonFile.

    PERFORM UNTIL WS-EOF = 'Y'
        READ PersonFile
            AT END
                MOVE 'Y' TO WS-EOF
            NOT AT END
                DISPLAY '------'
                DISPLAY 'NAME: ' Name
                DISPLAY 'EMAIL: ' Email
                DISPLAY 'AGE: ' Age
                DISPLAY 'COMPANY: ' Company
                DISPLAY 'city: ' City
        END-READ
    END-PERFORM.

    CLOSE PersonFile.
    STOP RUN.
