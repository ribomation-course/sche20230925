package ribomation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ribomation.api.Codec;
import ribomation.api.Record;
import ribomation.api.Value;
import ribomation.domain.Person;

import static org.junit.jupiter.api.Assertions.*;

class CodecTest {
    Codec target;

    @BeforeEach
    void setup() {
        target = new Codec();
    }

    @Record
    static class Dummy {
        @Value(size = 6)
        String name;

        @Value(size = 6)
        String company;

        public Dummy() {}
        public Dummy(String name, String company) {
            this.name = name;
            this.company = company;
        }
    }

    @Record
    static class Dummy2 {
        @Value(size = 6, pad = '#')
        String fname;

        @Value(size = 6, pad = '.')
        String lname;

        public Dummy2() {}
        public Dummy2(String fname, String lname) {
            this.fname = fname;
            this.lname = lname;
        }
    }

    @Test
    @DisplayName("Encode a dummy object")
    void encode() {
        var dummy = new Dummy("John", "IBM");
        var actual = target.encode(dummy);
        var expected = "John  IBM   ";
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Encode a dummy2 object")
    void encode2() {
        var dummy = new Dummy2("Alf", "Ek");
        var actual = target.encode(dummy);
        var expected = "Alf###Ek....";
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Encode a person object")
    void encodePerson() {
        var person = new Person("Nisse", "nisse@telia.com", 42, "Telia", "Stockholm");
        var expected = "Nisse           nisse@telia.com     42  Telia             Stockholm   ";
        assertEquals(expected, target.encode(person));
    }


    @Test
    @DisplayName("Decode a dummy object")
    void decode() throws Exception {
        var input = "John  IBM   ";
        var obj = target.decode(input, Dummy.class);
        assertNotNull(obj);
        assertEquals(Dummy.class, obj.getClass());
        assertEquals("John", obj.name);
        assertEquals("IBM", obj.company);
    }

    @Test
    @DisplayName("Decode a dummy2 object")
    void decode2() throws Exception {
        var input = "Alf###Ek....";
        var obj = target.decode(input, Dummy2.class);
        assertNotNull(obj);
        assertEquals(Dummy2.class, obj.getClass());
        assertEquals("Alf", obj.fname);
        assertEquals("Ek", obj.lname);
    }

    @Test
    @DisplayName("Decode a person object")
    void decodePerson() throws Exception {
        var input = "Nisse           nisse@telia.com     42  Telia             Stockholm   ";
        var obj = target.decode(input, Person.class);
        assertNotNull(obj);
        assertEquals("Nisse", obj.name);
        assertEquals("nisse@telia.com", obj.email);
        assertEquals(42, obj.age);
        assertEquals("Telia", obj.company);
        assertEquals("Stockholm", obj.city);
    }

}
