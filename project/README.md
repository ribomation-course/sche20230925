# Litet demo program: COBOL vs. Java

## COBOL
COBOL koden finns i `src/cobol/simple-read.cob` och läser en fil
med några personuppgifter och skriver ut dem på skärmen.

### Kompilering & Exekvering

#### Steg 1
Först behöver man ha WLS2 installerat, vilket beskrivs här:

* https://learn.microsoft.com/en-us/windows/wsl/install

#### Steg 2
Sen, installera Ubuntu Linux, via Microsoft Store, och följ instruktionerna.
![MS Store - Ubuntu](img/ms-store.png)

#### Steg 3
Öppna Ubuntu och installera GNU COBOL
```bash
sudo apt update
sudo apt install gnucobol
```

#### Steg 4
Kompilera COBOL programmet, genom att först navigera till project mappen
och skriva in följande kommando

```bash
cobc --free -x -o bin/simple-read src/cobol/simple-read.cob
```

Resultatet blir en körbar fil i `bin/simple-read`

#### Steg 5
Kör programmet med följande kommando

```bash
./bin/simple-read 
```

## Java
Java koden finns i `src/java/SimpleRead.java` och fungerar på samma sätt som COBOL programmet.

### API
I package `ribomation/api` finns två annoteringar för att markera en klass som en record
och vilka variabler som utgörs fält i recorden.

```java
@Record 
public class Person {
    @Value(size = 10) public String name;
    @Value(size= 4, pad = '.') public int    age;
}
```

Sen, finns det en `Codec` klass, som kan konverterar mellan ett Java objekt och en COBOL fil.
Notera, att det är en _mycket_ enkel och ofullständig implementation, som bara fungerar för
String och int fält.

### Domain
I package `ribomation/domain` finns en annoterad `Person` klass, tillsammans med en generator
klass, vilken kan generera Person objekt med fake data. Denna används av klassen 
`GenerateCobolFile`, som kan generera en `persons.dat` med önskat antal personer.

### Kompilering & Exekvering
Öppna projektet i IntelliJ IDEA och kör `GenerateCobolFile` först, för att skapa en `persons.dat` fil.
Kör sen COBOL programmet och Java programmet, och jämför resultatet.

